unit ll;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  generic TNode<T> = class
  type PNode = ^TNode<T>
    private
    	prev: PNode;
    	Item: T;
    	next: PNode;
    public
      procedure Append(_Item: T);
      procedure Find(_Item: T);
      procedure Delete(_Item: T);
  end;

implementation

end.

