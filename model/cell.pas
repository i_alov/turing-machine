unit cell;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls;

type
  PCell = ^TCell;
  TCell = object
    private
    symbol : char;
    shape : TShape;
    public
    procedure setSym(c: char);
    procedure setShape(s: TShape);
    function getSym(): char;
    function getShape() :TShape;
    constructor Create(c: char);
    destructor destroy();
  end;

implementation

procedure TCell.setSym(c: char);
begin
  self.symbol := c;
end;

procedure TCell.setShape(s: TShape);
begin
  self.shape := s;
end;

function TCell.getSym() : char;
begin
  result := self.symbol;
end;

function TCell.getShape(): TShape;
begin
  result := self.shape;
end;

constructor TCell.Create(c: char);
begin
  self.symbol := c;
  self.shape := nil;
end;

destructor TCell.Destroy();
begin
  self.shape.Destroy;
end;

end.

